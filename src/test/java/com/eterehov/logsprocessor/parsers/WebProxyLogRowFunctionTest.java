package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.utils.SearchUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class WebProxyLogRowFunctionTest {

    private static final int ITEM_INDEX = 1;

    private static WebProxyLogRowFunction logRowFunction;

    @BeforeClass
    public static void setUp() {
        logRowFunction = new WebProxyLogRowFunction(ITEM_INDEX);
    }

    @Test
    public void test_Null() {
        String logRow = null;

        String item = SearchUtils.findFirst(logRow, 0, logRowFunction);

        assertNull(item);
    }

    @Test
    public void test_Empty() {
        String logRow = null;

        String item = SearchUtils.findFirst(logRow, 0, logRowFunction);

        assertNull(item);
    }

    @Test
    public void test_SingleItem() {
        String logRow = "zero";

        String item = SearchUtils.findFirst(logRow, 0, logRowFunction);

        assertNull(item);
    }

    @Test
    public void test_LastItem() {
        String logRow = "zero one";

        String item = SearchUtils.findFirst(logRow, 0, logRowFunction);

        assertEquals("one", item);
    }

}
