package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.exception.ParserException;
import org.junit.Test;

public class WebProxyLogParserTest {

    private static final String FIELDS_PREFIX = "#Fields: ";
    private static final String HOST_FIELD = "cs-host";

    @Test(expected = ParserException.class)
    public void test_Init_IncorrectFormat_Null() {
        new WebProxyLogParser(null, FIELDS_PREFIX, HOST_FIELD);
    }

    @Test(expected = ParserException.class)
    public void test_Init_IncorrectFormat_Header() {
        new WebProxyLogParser("#WrongHeader: cs-uri-scheme cs-host cs-uri-port cs-uri-path", FIELDS_PREFIX, HOST_FIELD);
    }

    @Test(expected = ParserException.class)
    public void test_Init_IncorrectFormat_NoHost() {
        new WebProxyLogParser(FIELDS_PREFIX + "cs-uri-scheme cs-hist cs-uri-port cs-uri-path", FIELDS_PREFIX, HOST_FIELD);
    }

}
