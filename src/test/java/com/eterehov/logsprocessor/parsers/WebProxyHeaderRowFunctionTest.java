package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.utils.SearchUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class WebProxyHeaderRowFunctionTest {

    private static final String HEADER_ROW_PREFIX = "#Fields: ";
    private static final int DATA_OFFSET = HEADER_ROW_PREFIX.length();
    private static final String HOST_FIELD = "cs-host";

    private static WebProxyHeaderRowFunction headerRowFunction;

    @BeforeClass
    public static void setUp() {
        headerRowFunction = new WebProxyHeaderRowFunction(HOST_FIELD);
    }

    @Test
    public void test_Null() {
        String fieldsRow = null;

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNull(hostIndex);
    }

    @Test
    public void test_NoHost() {
        String fieldsRow = "#Fields: cs-uri-scheme cs-uri-path";

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNull(hostIndex);
    }

    @Test
    public void test_HostSingle() {
        String fieldsRow = "#Fields: " + HOST_FIELD;

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNotNull(hostIndex);
        assertEquals(0, hostIndex.intValue());
    }

    @Test
    public void test_HostFirst() {
        String fieldsRow = "#Fields: " + HOST_FIELD + " cs-uri-scheme cs-uri-path";

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNotNull(hostIndex);
        assertEquals(0, hostIndex.intValue());
    }

    @Test
    public void test_HostInTheMiddle() {
        String fieldsRow = "#Fields: cs-uri-scheme " + HOST_FIELD + " cs-uri-path";

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNotNull(hostIndex);
        assertEquals(1, hostIndex.intValue());
    }

    @Test
    public void test_HostLast() {
        String fieldsRow = "#Fields: cs-uri-scheme cs-uri-path " + HOST_FIELD;

        Integer hostIndex = SearchUtils.findFirst(fieldsRow, DATA_OFFSET, headerRowFunction);

        assertNotNull(hostIndex);
        assertEquals(2, hostIndex.intValue());
    }

}
