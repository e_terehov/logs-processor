package com.eterehov.logsprocessor.utils;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SearchUtilsTest {

    private static final String FIELD_VALUE = "secret";
    private static Function<String, Integer> function;

    @BeforeClass
    public static void setUp() {
        function = (itemsRow, itemValueStartIndex, itemValueEndIndex, itemIndex) -> {
            if (FIELD_VALUE.equals(itemsRow.substring(itemValueStartIndex, itemValueEndIndex))) {
                return itemIndex;
            }

            return null;
        };
    }

    @Test
    public void test_findFirst_SingleItem() {
        String fieldsRow = FIELD_VALUE;

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(0, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_SingleItem_SpacesBefore() {
        String fieldsRow = "   " + FIELD_VALUE;

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(0, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_SingleItem_SpacesAfter() {
        String fieldsRow = FIELD_VALUE + "   ";

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(0, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_MultipleItems_First() {
        String fieldsRow = FIELD_VALUE + " next_to_last last";

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(0, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_MultipleItems_Middle() {
        String fieldsRow = "first " + FIELD_VALUE + " last";

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(1, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_MultipleItems_Last() {
        String fieldsRow = "first second " + FIELD_VALUE;

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(2, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_MultipleSpaces() {
        String fieldsRow = "  first  " + FIELD_VALUE + "  last  ";

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(1, itemIndex.intValue());
    }

    @Test
    public void test_findFirst_SpaceaInQuotes() {
        String fieldsRow = "  \" f i r s t \"  " + FIELD_VALUE + "  \"  l  a  s  t  \"  ";

        Integer itemIndex = SearchUtils.findFirst(fieldsRow, 0, function);

        assertNotNull(itemIndex);
        assertEquals(1, itemIndex.intValue());
    }

}
