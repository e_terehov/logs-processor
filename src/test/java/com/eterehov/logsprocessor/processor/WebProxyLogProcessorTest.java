package com.eterehov.logsprocessor.processor;

import com.eterehov.logsprocessor.processors.WebProxyLogProcessor;
import com.eterehov.logsprocessor.processors.input.UrlDataSource;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class WebProxyLogProcessorTest {

    private WebProxyLogProcessor logProcessor;

    @Before
    public void setUp() {
        logProcessor = new WebProxyLogProcessor();
    }

    @Test
    public void test_processDataSource_SingleThread() {
        URL webProxyTestUrl = WebProxyLogProcessorTest.class.getResource("web_proxy_test.log");
        UrlDataSource dataSource = new UrlDataSource(webProxyTestUrl);

        logProcessor.processDataSource(dataSource);

        Map<String, Integer> stats = logProcessor.getStats();

        assertEquals(3, stats.size());
        assertThat(stats, Matchers.hasEntry("www.shukumaku.com", 1));
        assertThat(stats, Matchers.hasEntry("personal.avira-update.com", 2));
        assertThat(stats, Matchers.hasEntry("kh.google.com", 3));
    }

    @Test
    public void test_processDataSource_MultipleThreads() throws InterruptedException {
        URL webProxyTestUrl = WebProxyLogProcessorTest.class.getResource("web_proxy_test.log");
        UrlDataSource dataSource = new UrlDataSource(webProxyTestUrl);

        ExecutorService executorService = Executors.newFixedThreadPool(16);
        for (int i = 0; i < 100; i++) {
            executorService.submit(() -> logProcessor.processDataSource(dataSource));
        }

        executorService.shutdown();
        executorService.awaitTermination(60, TimeUnit.SECONDS);

        Map<String, Integer> stats = logProcessor.getStats();

        assertEquals(3, stats.size());
        assertThat(stats, Matchers.hasEntry("www.shukumaku.com", 100));
        assertThat(stats, Matchers.hasEntry("personal.avira-update.com", 200));
        assertThat(stats, Matchers.hasEntry("kh.google.com", 300));
    }

}
