package com.eterehov.logsprocessor;

import com.eterehov.logsprocessor.processors.WebProxyLogProcessor;
import com.eterehov.logsprocessor.processors.input.FileDataSource;
import com.eterehov.logsprocessor.processors.input.UrlDataSource;
import com.eterehov.logsprocessor.utils.PrintUtils;

import java.util.Map;

public class ParseLogs {

    public static void main(String[] args) {
        WebProxyLogProcessor logProcessor = new WebProxyLogProcessor();

        // processing from file
        if (args.length > 0) {
            // processing from file
            String fileName = args[0];
            FileDataSource fileDataSource = new FileDataSource(fileName);
            logProcessor.processDataSource(fileDataSource);
        }

        // processing from url
        UrlDataSource urlDataSource = new UrlDataSource("http://eterehov.com/remote_logs.log");
        logProcessor.processDataSource(urlDataSource);

        Map<String, Integer> statsMap = logProcessor.getStats();
        PrintUtils.sortAndPrint(statsMap);
    }

}
