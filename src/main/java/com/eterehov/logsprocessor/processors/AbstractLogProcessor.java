package com.eterehov.logsprocessor.processors;

import com.eterehov.logsprocessor.exception.ParserException;
import com.eterehov.logsprocessor.processors.input.AbstractDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

// todo: make process* to return Future
public abstract class AbstractLogProcessor {

    /**
     * Current stats of logs processing.
     *
     * @return logs processing stats
     */
    public abstract Map<String, Integer> getStats();

    /**
     * Processes logs from the datasource.
     *
     * @param dataSource dataSource to process
     */
    public void processDataSource(AbstractDataSource dataSource) {
        try (InputStream inputStream = dataSource.openInputStream()) {
            processStream(inputStream);
        } catch (IOException e) {
            throw new ParserException("Failed read data from DataSource", e);
        }
    }

    /**
     * Processes inputStream. Extracts logs, calculates stats.
     *
     * @param inputStream stream to process
     */
    protected abstract void processStream(InputStream inputStream);

}
