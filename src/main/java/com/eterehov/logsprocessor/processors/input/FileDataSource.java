package com.eterehov.logsprocessor.processors.input;

import com.eterehov.logsprocessor.exception.ParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * File data source for log processor.
 */
public class FileDataSource extends AbstractDataSource {

    private File file;

    public FileDataSource(String file) {
        this.file = new File(file);
    }

    public FileDataSource(File file) {
        this.file = file;
    }

    @Override
    public InputStream openInputStream() {
        try {
            return new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new ParserException("File not found " + file, e);
        }
    }

}
