package com.eterehov.logsprocessor.processors.input;

import java.io.InputStream;

/**
 * Abstract data source for log processor.
 */
public abstract class AbstractDataSource {

    /**
     * Opens InputStream for the provided DataSource
     *
     * @return opened InputStream
     */
    public abstract InputStream openInputStream();

}
