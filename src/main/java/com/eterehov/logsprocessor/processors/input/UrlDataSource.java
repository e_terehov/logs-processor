package com.eterehov.logsprocessor.processors.input;

import com.eterehov.logsprocessor.exception.ParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Url data source for log processor.
 */
public class UrlDataSource extends AbstractDataSource {

    private URL url;

    public UrlDataSource(String url) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            throw new ParserException("Incorrect format of the url " + url, e);
        }
    }

    public UrlDataSource(URL url) {
        this.url = url;
    }

    @Override
    public InputStream openInputStream() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new ParserException("Failed to open URL " + url, e);
        }
    }

}
