package com.eterehov.logsprocessor.processors;

import com.eterehov.logsprocessor.exception.ParserException;
import com.eterehov.logsprocessor.parsers.WebProxyLogParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class WebProxyLogProcessor extends AbstractLogProcessor {

    private static final String FIELDS_PREFIX = "#Fields: ";
    private static final String HOST_FIELD = "cs-host";

    private ConcurrentHashMap<String, AtomicInteger> statsMap;

    public WebProxyLogProcessor() {
        statsMap = new ConcurrentHashMap<>(1024);
    }

    @Override
    protected void processStream(InputStream inputStream) {
        try (
                InputStreamReader streamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(streamReader);
        ) {
            WebProxyLogParser logParser = null;

            String logRow;
            // reading log line-by-line
            while ((logRow = bufferedReader.readLine()) != null) {
                if (logParser == null) {
                    // finding headers rows first
                    if (logRow.startsWith(FIELDS_PREFIX)) {
                        try {
                            logParser = new WebProxyLogParser(logRow, FIELDS_PREFIX, HOST_FIELD);
                        } catch (Exception e) {
                            throw new ParserException("Incorrect header row format: " + logRow, e);
                        }
                    } else {
                        // todo: decide what to do with skipped rows
                    }
                } else {
                    // extracting host name
                    String hostName = logParser.parseItem(logRow);
                    if (hostName == null) {
                        // todo: incorrect row format case, need to be handled
                        continue;
                    }

                    // updating stats
                    statsMap.compute(hostName, (key, value) -> {
                        if (value == null) {
                            value = new AtomicInteger(1);
                        } else {
                            value.getAndIncrement();
                        }
                        return value;
                    });
                }
            }
        } catch (IOException e) {
            throw new ParserException("Failed to read data", e);
        }
    }

    @Override
    public Map<String, Integer> getStats() {
        // returning statsMap copy with Integers as values
        return statsMap.entrySet()
                .parallelStream()
                .collect(Collectors.toMap(Map.Entry::getKey, value -> value.getValue().get()));
    }

}
