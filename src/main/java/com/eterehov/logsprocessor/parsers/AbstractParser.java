package com.eterehov.logsprocessor.parsers;

public abstract class AbstractParser<I, O> {

    /**
     * Parses the item.
     *
     * @param item item to parse
     * @return parse result
     */
    public abstract O parseItem(I item);

}
