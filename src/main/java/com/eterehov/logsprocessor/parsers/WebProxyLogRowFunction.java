package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.utils.Function;

public class WebProxyLogRowFunction implements Function<String, String> {

    private final int hostItemIndex;

    /**
     * Extracts host value by its hostItemIndex
     *
     * @param hostItemIndex index of host value in the row
     */
    public WebProxyLogRowFunction(int hostItemIndex) {
        this.hostItemIndex = hostItemIndex;
    }

    @Override
    public String apply(String itemsRow, int itemValueStartIndex, int itemValueEndIndex, int itemIndex) {
        return itemIndex == this.hostItemIndex ? itemsRow.substring(itemValueStartIndex, itemValueEndIndex) : null;
    }

}
