package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.utils.Function;

public class WebProxyHeaderRowFunction implements Function<String, Integer> {

    private final String hostField;

    /**
     * Finds the index of the hostField in the header row
     *
     * @param hostField hostField value
     */
    public WebProxyHeaderRowFunction(String hostField) {
        this.hostField = hostField;
    }

    @Override
    public Integer apply(String itemsRow, int itemValueStartIndex, int itemValueEndIndex, int itemIndex) {
        // Compares char-by-char hostField value with lookup item. Returns itemIndex if succeed.
        if (itemValueEndIndex - itemValueStartIndex == hostField.length()) {
            for (int k = 0; k < hostField.length(); k++) {
                if (hostField.charAt(k) != itemsRow.charAt(itemValueStartIndex + k)) {
                    return null;
                }
            }

            return itemIndex;
        }

        return null;
    }

}
