package com.eterehov.logsprocessor.parsers;

import com.eterehov.logsprocessor.exception.ParserException;
import com.eterehov.logsprocessor.utils.Function;
import com.eterehov.logsprocessor.utils.SearchUtils;

/**
 * Custom Web Proxy Log Parser
 */
public class WebProxyLogParser extends AbstractParser<String, String> {

    private final Function<String, String> logRowParseFunction;

    public WebProxyLogParser(String fieldsRow, String fieldsPrefix, String hostField) {
        if (fieldsRow == null || !fieldsRow.startsWith(fieldsPrefix)) {
            throw new ParserException("Fields Row must start with \"" + fieldsPrefix + "\"");
        }

        WebProxyHeaderRowFunction fieldsRowParseFunction = new WebProxyHeaderRowFunction(hostField);
        Integer hostIndex = SearchUtils.findFirst(fieldsRow, fieldsPrefix.length(), fieldsRowParseFunction);

        if (hostIndex == null) {
            throw new ParserException("Fields Row must contain field \"" + fieldsPrefix + "\"");
        }

        this.logRowParseFunction = new WebProxyLogRowFunction(hostIndex);
    }

    @Override
    public String parseItem(String logRow) {
        return SearchUtils.findFirst(logRow, 0, logRowParseFunction);
    }

}
