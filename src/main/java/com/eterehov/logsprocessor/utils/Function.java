package com.eterehov.logsprocessor.utils;

public interface Function<I, O> {

    /**
     * Applies this Function to the given arguments.
     *
     * @param itemsRow            raw input value
     * @param itemValueStartIndex processing item start index
     * @param itemValueEndIndex   processing item end index
     * @param itemIndex           processing item index
     * @return the function result
     */
    O apply(I itemsRow, int itemValueStartIndex, int itemValueEndIndex, int itemIndex);

}
