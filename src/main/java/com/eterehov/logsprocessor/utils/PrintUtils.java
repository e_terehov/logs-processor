package com.eterehov.logsprocessor.utils;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PrintUtils {

    private static Comparator<Map.Entry<String, Integer>> comparator;

    static {
        comparator = (o1, o2) -> {
            if (o1.getValue().compareTo(o2.getValue()) == 0) {
                return o1.getKey().compareTo(o2.getKey());
            }

            return o2.getValue().compareTo(o1.getValue());
        };
    }

    public static void sortAndPrint(Map<String, Integer> statsMap) {
        List<Map.Entry<String, Integer>> sortedValues = statsMap.entrySet()
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());

        for (Map.Entry<String, Integer> entry : sortedValues) {
            System.out.printf("Host: %s, Count: %d%n", entry.getKey(), entry.getValue());
        }
    }

}
