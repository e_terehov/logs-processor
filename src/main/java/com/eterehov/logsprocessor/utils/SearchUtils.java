package com.eterehov.logsprocessor.utils;

public class SearchUtils {

    /**
     * Searches the fields row according to the following conditions:
     * <ul>
     * <li>The line is white space-delimited (a single or more space between one field to the other).</li>
     * <li>Fields that contains space in their value are surrounded by double quotes.</li>
     * </ul>
     *
     * @param fieldsRow      raw fields row
     * @param dataOffset     data offset in the fields row (everything before dataOffset is ignored)
     * @param searchFunction search function applied to each item
     * @param <O>            Output type
     * @return the first found item or null
     */
    public static <O> O findFirst(String fieldsRow, int dataOffset, Function<String, O> searchFunction) {
        if (fieldsRow == null) {
            return null;
        }

        // indicated if we looking over item (one+ spaces)
        boolean processingItem = false;

        // indicated if we iterating over item surrounded by double quotes
        boolean processingQuotes = false;

        int itemIndex = 0;
        int itemValueStartIndex = dataOffset;

        O outValue = null;
        // iterating chars one-by-one starting from the offset position
        for (int i = dataOffset; i < fieldsRow.length(); i++) {
            boolean isLast = (i == fieldsRow.length() - 1);

            if (fieldsRow.charAt(i) != ' ' && !processingItem) {
                // starting item processing
                processingItem = true;
                itemValueStartIndex = i;
            }

            if (processingItem && !processingQuotes && (fieldsRow.charAt(i) == ' ' || isLast)) {
                int itemValueEndIndex = (isLast && fieldsRow.charAt(i) != ' ') ? i + 1 : i;

                // applying searchFunction on current item
                outValue = searchFunction.apply(fieldsRow, itemValueStartIndex, itemValueEndIndex, itemIndex);
                // todo: do not return quotes for quoted value
                if (outValue != null) {
                    break;
                }

                // completing item processing
                itemIndex++;
                processingItem = false;
            }

            // updating processingQuotes flag whenever quotes found
            if (fieldsRow.charAt(i) == '\"') {
                processingQuotes = !processingQuotes;
                // todo: handle escaped quotes in qoutes case
            }

        }

        return outValue;
    }

}
